<?php

use Illuminate\Support\Facades\Route;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', 'ProductsController@index');
Route::get('/categories', 'CategoryController@index');
Route::post('/add-product', 'ProductsController@product');
Route::post('/add-category', 'CategoryController@addCatgory');
Route::post('/category/change-status', 'CategoryController@changeStatus');
Route::post('/category/delete', 'CategoryController@deleteCategory');
Route::post('/product/delete', 'ProductsController@deleteProduct');
