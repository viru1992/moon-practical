/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$('.addUpdateCategoryForm').on('submit', function (e) {
    e.preventDefault();
    var url = $(this).attr('action');
    $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: $(this).serialize(),
        success: function (data) {
            if (data.status == 'success') {
                alert(data.message);
                window.location.href = './categories';
            }
        }
    })
});

$(document).on('click', '.changeCategoryStatus', function (e) {
    e.preventDefault();
    var currentButton = $(this);
    var url = $(this).attr('href');
    var status = $(this).data('status');
    var id = $(this).data('id');
    var csrf = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: {'status': (status == 1) ? 0 : 1, id: id, "_token": csrf},
        success: function (data) {
            if (data.status == 'success') {
                currentButton.data('status', (status == 1) ? 0 : 1);
                currentButton.text((status == 1) ? 'Deactive' : 'Active');
            }
        }
    })
});
$(document).on('click', '.deleteCategory', function (e) {
    e.preventDefault();
    var url = $(this).attr('href');
    var id = $(this).data('id');
    var csrf = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: {'status': (status == 1) ? 0 : 1, id: id, "_token": csrf},
        success: function (data) {
            if (data.status == 'success') {
                $('.delete-' + id).remove();
                window.location.href = './categories';
            }
        }
    })
});

$(document).on('click', '.deleteProduct', function (e) {
    e.preventDefault();
    var url = $(this).attr('href');
    var id = $(this).data('id');
    var csrf = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: {'status': (status == 1) ? 0 : 1, id: id, "_token": csrf},
        success: function (data) {
            if (data.status == 'success') {
                $('.delete-' + id).remove();
            }
        }
    })
});


$('.addUpdateProduct').on('submit', function (e) {
    e.preventDefault();
    var url = $(this).attr('action');
    var formData = new FormData(this);
    $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            console.log(data);
            if (data.status == 'success') {
                alert(data.message);
                window.location.href = './';
            }
        }
    })
});

$(document).ready(function () {
    $(".categor-list").click(function (event) {
        event.stopPropagation();
        $(this).children('ul:first').toggle();
    });
    $(".sub-categor-list").click(function (event) {
        event.stopPropagation();
        $(this).children('ul:first').toggle();
    });
})
