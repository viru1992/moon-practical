<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_category extends Model {

    public $table = 'products_category';
    protected $fillable = [
        'id', 'category_id', 'product_id'];

    public function category() {
        return $this->hasOne('App\Categories', 'id', 'category_id');
    }

    public static function addProductCategory($data) {
        $cat = Product_category::create($data);
        return $cat;
    }

}
