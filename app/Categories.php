<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Categories extends Model {

    public $table = 'categories';
    protected $fillable = [
        'id', 'category', 'parent_id', 'isActive', 'isDelete'];

    public function subcategory() {
        return $this->hasMany('App\Categories', 'parent_id');
    }

    public static function getCategories() {
        $categories = Categories::select()
                ->where(['isDelete' => false])
                ->get();
        return $categories;
    }
    public static function getCategory($id) {
        $categories = Categories::find($id)
                ->where(['isDelete' => false]);
        return $categories;
    }

    public static function getSideBarCategories() {
        $categories = Categories::select()
                ->where(['isDelete' => false, 'parent_id' => 0])
                ->get();
        return $categories;
    }

    public static function addCategories($catData) {
        $cat = Categories::create([
                    'category' => $catData['category'],
                    'parent_id' => ($catData['parent_id']) ? $catData['parent_id'] : 0
        ]);
        return $cat;
    }

    public static function changeStatus($data) {
        $cat = Categories::find($data['id']);
        $cat->isActive = $data['status'];
        $cat->save();
        return true;
    }
    public static function deleteCategory($id) {
        $cat = Categories::find($id);
        $cat->isDelete = true;
        $cat->save();
        return true;
    }

}
