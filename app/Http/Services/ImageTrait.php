<?php

namespace App\Http\Services;

use URL;

trait ImageTrait {

    //api image
    public function addImage($image, $attribute, $num = 0) {
        $fileName = time() . $num . '.' . $image->getClientOriginalExtension();

        $imagePath = $image->storeAs('public/' . $attribute, $fileName);

        $imageresizePath = $image->storeAs('public/' . $attribute . '/tmp', $fileName);

        $img = \Image::make(storage_path() . '/app/' . $imagePath);
        // save thumbnail image
        $img->resize(200, 200, function ($constraint) {
            $constraint->aspectRatio();
        })->save(storage_path() . '/app/' . $imageresizePath);
        return $fileName;
    }

}
