<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categories;
use App\Products;
use App\Product_category;
use Auth;
use App\Http\Services\ResponseTrait;
use App\Http\Services\ImageTrait;

class ProductsController extends Controller {

    use ResponseTrait;
    use ImageTrait;

    public function index() {
        $categories = Categories::getCategories();
        $categorieside = Categories::getSideBarCategories();
        $products = Products::getProducts();
        return view('index', ['categories' => $categories, 'products' => $products, 'categorieside' => $categorieside]);
    }

    public function product(Request $request) {
        $data = $request->all();
        if ($request->hasFile('image')) {
            $data['image'] = $this->addImage($data['image'], 'product-image');
        } else {
            $data['image'] = '';
        }
        $product = [
            'name' => $data['productName'],
            'image' => $data['image'],
            'description' => $data['description'],
            'price' => $data['price']
        ];
        $addProduct = Products::addProducts($product);
        if ($addProduct) {
            foreach ($data['categories'] as $cat) {
                Product_category::addProductCategory(['product_id' => $addProduct, 'category_id' => $cat]);
            }
            return $this->responseJson('success', 'Product Added successfully', 200);
        }
    }

    public function deleteProduct(Request $request) {
        $data = $request->all();
        $delCat = Products::deleteProduct($data['id']);
        if ($delCat) {
            return $this->responseJson('success', 'Product Delete successfully', 200);
        } else {
            return $this->responseJson('error', 'Something went wrong', 201);
        }
    }

}
