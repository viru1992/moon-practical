<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categories;
use App\Http\Services\ResponseTrait;
use Auth;

class CategoryController extends Controller {

    use ResponseTrait;

    public function index() {
        $categories = Categories::getCategories();
        $categorieside = Categories::getSideBarCategories();
        return view('category', ['categories' => $categories, 'categorieside' => $categorieside]);
    }

    public function catgory() {
        return view('addCategory', ['categories' => $categories]);
    }

    public function addCatgory(Request $request) {
        $data = $request->all();
        $addCat = Categories::addCategories($data);
        if ($addCat) {
            return $this->responseJson('success', 'Category Added successfully', 200);
        } else {
            return $this->responseJson('error', 'Something went wrong', 201);
        }
    }

    public function changeStatus(Request $request) {
        $data = $request->all();
        $addCat = Categories::changeStatus($data);
        if ($addCat) {
            return $this->responseJson('success', 'Category Status Changed', 200);
        } else {
            return $this->responseJson('error', 'Something went wrong', 201);
        }
    }

    public function deleteCategory(Request $request) {
        $data = $request->all();
        $delCat = Categories::deleteCategory($data['id']);
        if ($delCat) {
            return $this->responseJson('success', 'Category Delete successfully', 200);
        } else {
            return $this->responseJson('error', 'Something went wrong', 201);
        }
    }

}
