<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model {

    public $table = 'products';
    protected $fillable = [
        'id', 'image', 'name', 'price', 'isActive', 'isDelete', 'description'];

    public function categories() {
        return $this->hasMany('App\Product_category', 'product_id', 'id');
    }
    

    public static function getProducts() {
        $products = Products::select()
                ->where(['isDelete' => false])
                ->get();
        return $products;
    }

    public static function addProducts($data) {
        $product = Products::create($data);
        return $product->id;
    }

    public static function deleteProduct($id) {
        $prod = Products::find($id);
        $prod->isDelete = true;
        $prod->save();
        return true;
    }

}
