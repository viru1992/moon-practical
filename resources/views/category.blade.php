@extends('layouts.main')

@section('content')
<div class="col-md-4 col-lg-4 col-sm-4">
    <div class="row">
        <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#addCategory">
            Add Category
        </button>
    </div>
    <div class="row">
        <table class="table-bordered">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th >category</th>
                    <th >parentId</th>
                    <th >Status</th>
                    <th >Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($categories as $cat)
                <tr class="delete-{{$cat->id}}">
                    <td scope="row">{{$cat->id}}</td>
                    <td>{{$cat->category}}</td>
                    <td>{{$cat->parent_id}}</td>
                    <td><a href="{{url('/category/change-status')}}" class="changeCategoryStatus" data-id="{{$cat->id}}" data-status="{{$cat->isActive}}">@if($cat->isActive == true) 
                            Active
                            @else
                            Deactive
                            @endif 
                        </a></td>
                    <td><a href="" data-id='{{$cat->id}}' class="editCategory" >edit</a> / <a href="{{url('/category/delete')}}" data-id='{{$cat->id}}' class="deleteCategory" >delete</a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="addCategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form action="{{url('/add-category')}}" class="addUpdateCategoryForm">
                        @csrf
                        <div class="form-group">
                            <label>name</label>
                            <input type="text" name="category" required=""/>
                        </div>
                        <div class="form-group">
                            <label>Categories</label>
                            <select class="select" name="parent_id">
                                <option value="">Select-Parent-Category</option>
                                @foreach($categories as $cat)
                                @if($cat->isActive == true)
                                <option value="{{$cat->id}}">{{$cat->category}}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Add Category</button>
                        </div>
                    </form> 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection