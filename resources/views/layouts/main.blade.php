<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Scripts -->


        <!-- Fonts -->
        <link rel="dns-prefetch" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src="{{ asset('public/js/app.js') }}" defer></script>
    </head>
    <body>
        <div id="app">
            <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
                <div class="container">
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>
            </nav>

            <main class="row">
                <div class="col-md-3 col-lg-3 col-sm-3">
                    <div class="col-md-12">
                        <h6>Menu</h6>    
                        <ul>
                            <li><a href="{{ url('/') }}">Products</a></li>
                            <li><a href="{{ url('/categories') }}">Categories</a></li>
                        </ul>
                    </div>
                    <div class="col-md-12">
                        <h6>Categories</h6>    
                        <ul>
                            @foreach($categorieside as $cat)
                            @if(count($cat->subcategory) > 0 && $cat->isDelete == false)
                            <li class="categor-list">
                                {{$cat->category}}({{count($cat->subcategory)}})
                                <ul style="display: none">
                                    @foreach($cat->subcategory as $subcat)
                                    @if(count($subcat->subcategory) > 0 && $subcat->isDelete == false)
                                    <li class="sub-categor-list">
                                        {{$subcat->category}}({{count($subcat->subcategory)}})
                                        <ul style="display: none">
                                            @foreach($subcat->subcategory as $subsub)
                                            @if($subsub->isDelete == false)
                                            <li>{{$subsub->category}}({{count($subsub->subcategory)}})</li>
                                            @endif
                                            @endforeach
                                        </ul>
                                    </li>
                                    @else
                                    <li class="sub-categor-list">{{$subcat->category}}({{count($subcat->subcategory)}})</li>
                                    @endif
                                    @endforeach
                                </ul>
                            </li>
                            @else
                            <li class="categor-list">
                                {{$cat->category}}({{count($cat->subcategory)}})
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>
                </div>

                @yield('content')
            </main>
        </div>
    </body>
</html>
