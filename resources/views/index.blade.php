@extends('layouts.main')

@section('content')
<div class="col-md-5 col-lg-5 col-sm-5">
    <div class="row">
        <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#addProduct">
            Add Product
        </button>
    </div>
    <div class="row">
        <table class="table-bordered">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">productName</th>
                    <th scope="col">price</th>
                    <th scope="col">description</th>
                    <th scope="col">categories</th>
                    <th scope="col">image</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($products as $product)
                <tr class="delete-{{$product->id}}">
                    <th scope="row">{{$product->id}}</th>
                    <td>{{$product->name}}</td>
                    <td>{{$product->price}}</td>
                    <td>{{$product->description}}</td>
                    <td>@foreach($product->categories as $cat)
                        {{$cat->category->category}} |
                        @endforeach</td>
                    <td><img src="{{ URL::to('storage/app/public/product-images/tmp/'.$product->image)}}"></td>
                    <td><a href="#" data-id='{{$product->id}}' class="" >edit</a> / <a href="{{url('/product/delete')}}" data-id='{{$product->id}}' class="deleteProduct" >delete</a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="addProduct" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form action="{{url('/add-product')}}" class="addUpdateProduct">
                        @csrf
                        <div class="form-group">
                            <label>name</label>
                            <input type="text" name="productName" required=/>
                        </div>
                        <div class="form-group">
                            <label>price</label>
                            <input type="text" name="price" required=""/>
                        </div>
                        <div class="form-group">
                            <label>Categories</label>
                            <select class="select" name="categories[]" multiple="" required="">
                                <option value="">Select-Categories</option>
                                @foreach($categories as $cat) 
                                @if($cat->isActive == true)
                                <option value="{{$cat->id}}">{{$cat->category}}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea name="description" required=""></textarea>
                        </div>
                        <div class="form-group">
                            <label>Image</label>
                            <input type="file" name="image" required=""/>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Add Product</button>
                        </div>
                    </form> 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection